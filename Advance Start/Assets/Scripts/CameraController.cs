﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    public Camera Camera;
    public GameObject Ball;

    private GameObject currentBall;
    private float rotateH = 0.0f;
    private float rotateV = 0.0f;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        rotateH += speedH * Input.GetAxis("Mouse X");
        rotateV -= speedV * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(rotateV, rotateH, 0.0f);

        if (Input.GetMouseButtonDown(0))
        {
            if (currentBall)
                Destroy(currentBall);
            currentBall = Instantiate(Ball, Camera.main.transform.position, transform.rotation);
        }
    }
}
