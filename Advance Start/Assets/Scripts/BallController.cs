﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    public Camera Camera;
    public float speed = 15;
    Vector3 direction;

    // Use this for initialization
    void Start()
    {
        /*
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            direction = hit.point;
        }
        */
        direction = Camera.main.transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * Time.deltaTime * speed);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "physic_piece" || 
            col.gameObject.name == "physic_piece (1)" || 
            col.gameObject.name == "physic_piece (2)")
        {
            col.gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
